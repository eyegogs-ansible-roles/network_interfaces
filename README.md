# Network interfaces role for Debian and FreeBSD

An ansible role for configuring network interfaces on Debian and FreeBSD.

> **Disclaimer**: This role is still a work in progress. As of the moment not all configurations are implemented for certain OSes, they are listed below.

## Configurations
- **Interface** - Plain interface, DHCP or static address
- **PPPoE** (FreeBSD only) - PPPoE configuration
- **Trunk** - Trunk interface for passing traffic for mutiple VLANs, creates subinterfaces on the host for each VLAN
- **Wireguard** - [Wireguard](https://www.wireguard.com/) VPN interfaces
- **Bridge** Bridge interfaces with slave physical ports and/or VLAN subinterfaces
- **Laggs** (FreeBSD only) - Aggregated interfaces

## Usage
Interfaces are defined in the `interfaces` dict. The key of each sub-dictionary is the name of an interface to configure on the host.

The common fields are described below:
- `interfaces`: Dict containing interface configurtion, the key of each subdictionary in `interfaces` is an interface to configure on the hosts.
    - `type`: The type of interface to configure, defaults to `interface`. Can be: `interface`, `trunk`, `wireguard`, `bridge` or `pppoe`.
    - `name_servers`: (List) A list of nameservers this interface should use for DNS
    - `cidr`: The [CIDR notation](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing#CIDR_notation) of the interfaces address and netmask
    - `dhcp`: Whether this interface obtains an address via [DHCP](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol). Overwrites `cidr` if set.
    - `gateway`: (**Debian only**) The default gateway associated with this interface. Setting different gateways on different interfaces requires seperate routing table and so **this should only be declared on a single interface**.
    - `broadcast`: (**Debian only**) The broadcast address of the subnet this interface is in

### Additional Debian variables
- `remove_undefined_ifaces`: By default the role will remove any network interfaces defined in `interfaces.d`. You can set this to `false` to disable this behaviour.

### Additional FreeBSD variables
Some addtional variables are used for FreeBSD outside of the `interfaces` dict:
- `gateway_enable`: Whether this host is enabled as a router. Defaults to `false`.
- `default_gateway`: The default gateway of this host.
- `resolvconf_enable`: A boolean which dicates whether `resolvconf` is enabled on boot. Set to false to set your own DNS information on the host.
- `search_domains`: A list of search domains that is inserted into `/etc/resolv.conf` when `resolvconf_enable` is `false`
- `nameservers`: A list of nameservers that is inserted into `/etc/resolv.conf` when `resolvconf_enable` is `false`

### Trunks/VLANs
- `subinterfaces`: (List) A list of subinterfaces to configure on the host. Most of the configuration for subinterfaces is the same as regular interfaces.
	- `vlan_id`: The VLAN id of this subinterface. The name of the subinterface created on the host is `<interface>.<vlan_id>`

### Bridges
- `bridge_ports`: A list of member interfaces or subinterfaces to attach to this bridge

### LAGGs (FreeBSD only)
- `member_ports`: A list of member interfaces to add to this LAGG.
- `subinterfaces`: Break out this lagg interface into VLANs. Same as `subinterfaces` for trunks - see above for details.

### PPPoE (FreeBSD only)

> At the time of writing, this role only supports a single PPPoE configuration on a host

This implementation can use either FreeBSDs [userland ppp](https://docs.freebsd.org/en/books/handbook/ppp-and-slip/#pppoe) or [mpd5](https://mpd.sourceforge.net/) in which negotiation happens in userland but routing and forwarding takes place in kernel space.
- `pppoe`: A dict containing information for PPPoE connections
	- `config_name`: The name of this pppoe profile if using pppd or the config name if using mpd5, a sensible name is the name of your ISP provider for instance.
        - `type`: Either `pppd` or `mpd`. Defaults to `pppd`
	- `username`: Username for PPPoE
	- `password`: Password for PPPoE, use of [`ansible-vault`](https://docs.ansible.com/ansible/latest/cli/ansible-vault.html) is recommended
	- `nat`: Whether or not to enable nat handling in `pppd` for this interface, defaults to `true`
    - `up_script`: (mpd5 only) Lines of a script (`sh`) represented as a list. The script is created at `/usr/local/etc/mpd5/up-script-<profile_name>.sh` and is invoked when the interface is brought up. As of the moment this script ALWAYS returns 0.

### Loopback (FreeBSD only)
- `cidr`: The [CIDR notation](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing#CIDR_notation) of the interface address and netmask

### Wireguard
- `wireguard`: A dict containing configuration of the wireguard interface and its connected peers
	- `listen_port`: [Optional] The port to listen on for incoming peer connections - if not set then wireguard selects a random one.
	- `private_key_file`: The file on the ansible controller containing the private key for this interface
	- `private_key`:  The private key for this interface. Use of [`ansible-vault`](https://docs.ansible.com/ansible/latest/cli/ansible-vault.html) is recommended. Takes precedence over `private_key_file`
	- `name_servers`: (List) Name servers connected peers should use for DNS
	- `peers`: (List) A list of peers on this wiregaurd interface
		- `public_key`: The public key for this peer
		- `allowed_ips`: (List) A list of IP address that are allowed to route through this interface
        - `endpoint`: [Optional] The endpoint of this peer. E.g. `192.168.0.254:2443`
        - `keepalive`: [Optional] The persistent keepalive interval for this peer.

### Routing tables

> Currently this feature is Debian-Only

Extra routing tables can be defined via the `fibs` dictionary:

```yaml
fibs:
  100:
    name: "extraroutes"
```

- Each sub-dictionary of fibs is the ID of the routing table/fib to create on the host.
- `name` is a meaningful name that can be given to each fib to be used in commands

## Examples
### Debian bridge example with VLANs
```yaml
---
interfaces:
  eno1:
    type: trunk
    subinterfaces:
      - vlan_id: 7
      - vlan_id: 3
      - vlan_id: 8
  br0:
    cidr: 10.7.1.10/16
    broadcast: 10.7.255.255
    name_servers:
      - 10.3.1.3
    type: bridge
    bridge_ports:
      - eno1.7
  br1:
    cidr: 10.3.1.2/16
    broadcast: 10.3.255.255
    gateway: 10.3.1.1
    name_servers:
      - 10.3.1.3
    type: bridge
    bridge_ports:
      - eno1.3
  br2:
    cidr: 10.8.1.20/16
    broadcast: 10.8.255.255
    name_servers:
      - 10.3.1.3
    type: bridge
    bridge_ports:
      - eno1.8
```

### FreeBSD router with external PPPoE interface, internal trunk and Wireguard
```yaml
---
gateway_enable: true
default_gateway: "82.160.17.42" # The address of igb0 obtained through PPPoE

interfaces:
  igb0:
    type: pppoe
    pppoe:
      config_name: "myisp"
	  nat: false # Disable NAT so it can be handled by pf
      up_script:
        - service pf reload
	  username: "myusername@myisp"
      password: !vault |
	      $ANSIBLE_VAULT;1.1;AES256
	      34623864383439393765333666386438333632653833626662363261656331363937323865636338
	      6361396334326663643036613761633365633462393636360a353834336265653131363631613662
	      6136
  igb1:
    type: trunk
    subinterfaces:
      - vlan_id: 2
        cidr: 10.50.2.254/24
      - vlan_id: 3
        cidr: 172.16.3.254/24
      - vlan_id: 4
        cidr: 10.50.4.254/24
      - vlan_id: 5
        cidr: 10.50.5.254/24
      - vlan_id: 99
        cidr: 10.50.99.254/24
  # User VPN
  wg0:
    type: wireguard
    cidr: "172.25.50.254/24"
    wireguard:
      listen_port: 5764
      private_key_file: files/vpn/server.key
      name_servers:
        - "10.10.50.254"
      peers:
	# SomeDudeA
        - public_key: "yvt++VSE/3zZH8COz4RN9bMPSpVoSzifITpXXKPX81k="
          allowed_ips:
            - "172.25.50.1/32"
	# SomeDudeB
        - public_key: "t6bevz4ZqZkVIvBzhEgQe80Azb7zeuPb20RWgf85qTI="
          allowed_ips:
            - "172.25.50.2/32"
	# SomeDudeC
        - public_key: "6TW+iI+PSZ7i9AxvHSjCARPXG6Jt3u9d1D0wElgPCjc="
          allowed_ips:
            - "172.25.50.3/32"
```

### FreeBSD server with static DNS and default route, LAGG interface, VLANs and bridge
```yaml
---
default_gateway: "10.35.4.254"
search_domains:
  - eyegog.co.uk
nameservers:
  - 10.35.4.254

interfaces:
  lagg0:
    type: lagg
    member_ports:
      - igb0
      - igb1
    subinterfaces:
      - vlan_id: 4
        cidr: "10.35.4.2/24"
      - vlan_id: 6
        cidr: "10.35.6.2/24"
      - vlan_id: 99
        cidr: "10.35.99.2/24"
  bridge0:
    type: bridge
    bridge_ports:
      - lagg0.4
```
